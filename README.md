# stage_environments #

Luca Iocchi, 2014-2021

stage_environments extends and simplifies the definition, management and execution of simulation environments with Stage in ROS.

The package contains:

* a modified version of stage_ros with parametric names of topics and frames
* a modified version of Stage and stage_ros with GUI controls set from ROS
* an extension to yaml files to add features to be loaded in the simulator
* a Python script for automatically creating worlds from map files and for
running stage, robots and navigation nodes (amcl, move_base, etc.)

----

## Quick start

To quickly start a demo, follow these steps.

* 1) Download

    Download this repository in `<path_to>/stage_environments`

* 2) Option 1 (faster): Standard settings

    In host terminal:

        cd <path_to>/stage_environments/docker
        docker pull iocchi/stage_environments
        ./run.bash


* 2) Option 2: Special settings

    If you have special settings on your host system (e.g., nvidia graphic cards), then you need to build the image locally, as follows

    In host terminal:

        cd <path_to>/stage_environments/docker
        ./build.bash [melodic|kinetic] [force-build-tag]
        ./run.bash [melodic|kinetic]

    Note: use `force-build-tag` with any tag to rebuild `Stage` and `stage_environments` (e.g., after a `git pull`)

* 3) Run

    Use GUI to start a simulated environment.


* 4) Use the container:

        docker exec -it stage_environments [bash|tmux]

        docker exec stage_environments bash -ci 'rosrun stage_environments start_simulation.py DISB1'

        ...

        docker exec stage_environments bash -ci 'rosrun stage_environments quit.sh'

* 5) Server mode

Run image with stage_environments in server mode

        ./run_server.bash

Send commands to the server

        echo '<map name>[;robot_type[;<num_robots>]]' | netcat -w 1 localhost 9235
        echo 'DISB1' | netcat -w 1 localhost 9235
        echo 'DISB1;marrtino' | netcat -w 1 localhost 9235
        echo 'DISB1;marrtino;2' | netcat -w 1 localhost 9235
        echo '@stagekill' | netcat -w 1 localhost 9235
        echo '@quitserver' | netcat -w 1 localhost 9235

----

## Install

### Local installation

Install ROS packages `stage_ros` and `map_server`

    sudo apt get install ros-kinetic-stage-ros ros-kinetic-map-server

Download this package in your ROS workspace and use `catkin_make` to build it.

### Docker installation

* Build the docker image

    There are two alternative options: 1) pull the image from docker hub, 2) build an image locally.

    1) Pull option: download the docker image from docker hub. Two versions available: 

    `Ubuntu 18.04/ROS melodic` (default) 

        docker pull iocchi/stage_environments

    `Ubuntu 16.04/ROS kinetic`

        docker pull iocchi/stage_environments:kinetic


    2) Local option: build the image from `Dockerfile`
    
    Run the script from the folder where you downloaded this repository
    
        ./docker_build.bash [melodic|kinetic]


* Run the docker image or create the container

    Run the image

        ./docker_run.bash [melodic|kinetic]


* [optional] Create a container

    To create a container

        ./docker_create.bash
 

    Start/stop container

        docker start stage_environments

        docker stop stage_environments

    Open a tmux shell in a running container

        docker exec -it stage_environments tmux


* [optional] Development

    To test development of `stage_environments` 

        ./docker_dev.bash
 
    will mount the current folder in the container.





## Use

* Configuration (not needed if you are using the docker image)

    Set `MAPSDIR` environment variable to folders containing maps (in ROS format)

    Example:

        export MAPSDIR=$HOME/src/stage_environments/maps:$MARRTINO_APPS_HOME/mapping/maps:$HOME/playground/maps


    ROS map YAML files can be augmented with semantic elements added in the Stage world.

    Examples:

        initposes: [ [11.8, 2, 270], [2, 2, 0] ]

        door1: { type: 'door', size: [0.7, 0.1, 1.0], pose: [ -0.3, 1.3, 0, -170 ], color: "brown" }
        door2: { type: 'door', size: [0.7, 0.1, 1.0], pose: [ 1.5, 0, 0, 0 ], color: "brown" }
        door3: { type: 'door', size: [0.7, 0.1, 1.0], pose: [ 9.35, -2.3, 0, -80 ], color: "brown" }

        john: { type: 'person', pose: [ 13, 2.4, 0, -170 ],  body: { color: "green" } }
        mary: { type: 'person', pose: [ 10.4, -4.4, 0, 95 ],  body: { color: "red" } }

        box1: { type: 'box', pose: [ 19.0, 1.0, 0, 45 ], color: "grey" }
        box2: { type: 'box', pose: [ 21.0, 1.5, 0, 0 ], color: "DarkGrey" }

        tag1: { type: 'tag', pose: [ 19.0, 1.0, 0, 45 ], color: "grey" }

        printer1: { 
            type: 'box', pose: [ 5.5, 3.1, 0, 0 ], color: 'grey', 
            light: { name: 'printerlight1', color: 'green', size: [0.2, 0.2, 0.05] } 
        }

    All these info are also available as ROS parameters within the namespace `/map_server`.
    Values refer to the intial state (they are not updated during the simulation) and have to be used in read-only mode.

    Examples:

        rosparam get /map_server/door1/pose
        [ -0.3, 1.3, 0, -170 ]

        rosparam get /map_server/mary/body/color
        red


* Command-line use:

        rosrun stage_environments start_simulation.py -h
        <<< shows available options >>>

        rosrun stage_environments start_simulation.py --list
        <<< shows list of available maps >>>

        rosrun stage_environments start_simulation.py <map_name>
        <<< starts the simulator with the specified map >>>

        ...

        rosrun stage_environments quit.sh
        <<< quits the simulator and kills all ROS nodes >>>

* NO GUI simulator

    To run stage simulator without GUI, use option `--no_gui`

        rosrun stage_environments start_simulation.py --no_gui <map_name>


* GUI based use:

    See command line options with

        rosrun stage_environments start_simulation.py 


* Adding new environments

    To add a new environment, just add the two files `<map>.yaml` and `<map>.png` output of a mapping process  in a folder included in the env variable `MAPSDIR`. You do not need to write the corresponding world file for Stage, as they will be automatically generated by the `start_simulation.py` script.


* Stage GUI controls

Note: these features are available only with a customized version of Stage, 
which is available in `https://github.com/iocchi/Stage`. This version is included in the docker image.

Send a String topic to `/stageGUIRequest` with any of the following

    screenshot          : activate screenshots for 2 seconds
    footprints          : activate footprints
    footprints_off      : deactivate footprints
    speedup_<value>     : set simulation speedup to <value>
    pause               : pause the simulation
    resume              : resume the simulation

----

## Running Details

* ROS nodes running

    When launching the simulation, the following ROS nodes will be launched

        stage_ros   # stage simulator
        map_server  # map server for the localizer


    Topics published (stageros sends messages)

        map                     # map of the environment
        map_metadata            # map data
        odom                    # robot odometry
        scan                    # robot laser scan readings
        camera_info             # robot camera info
        image                   # robot color image
        depth                   # robot depth image
        base_pose_ground_truth  # robot pose ground truth
        people                  # ground truth poses of people
        tf                      # ROS tf

    Topics subscribed (stageros receives messages)

        cmd_vel                 # robot cmd_vel
        stage_say               # robot speech
        setpose                 # robot set global pose
        <personname>/cmd_vel    # person cmd_vel
        <personname>/stage_say  # person speech
        <personname>/setpose    # person set global pose
        <lightname>/state       # light state
        <lightname>/color       # light color
        stageGUIrequest         # commands for stage GUI control

    Frames published

        map                     # map frame
        odom                    # odom frame
        base_frame              # base frame
        base_footprint_frame    # base footprint frame
        laser_frame             # laser frame
        camera_frame            # camera frame

    Parameters available

        stalled                 # [boolean] model is stalled (i.e., crashed into another model)
        map_server/<map info>   # all values in the map yaml file
        <lightname>/state       # light state (read-only use)
        <lightname>/color       # light color (read-only use)


    These default values can be changed in the configuration files.

    When people or multiple robots are launched, the topics are prefixed with the robot/person names specified in the `yaml` file.

    Example of named topics

        <robotname>/odom
        <robotname>/cmd_vel
        <robotname>/scan

        <personname>/odom
        <personname>/cmd_vel


    People can be moved as well as robots through `<personname>/cmd_vel` topic.

    By default, no localization system is running, so there is no mapping from `odom` to `map`. 
    To run localization, navigation and other nodes you need to install the corresponding ROS packages (that are not included in this package) and launch the corresponding nodes.

    Lights can be modified through topics

        <lightname>/state
        <lightname>/color

    To change light status

        rostopic pub /<lightname>/state std_msgs/Int8 "data: 1" --once
        rostopic pub /<lightname>/state std_msgs/Int8 "data: 0" --once

    To change light color

        rostopic pub /<lightname>/color std_msgs/String "data: 'yellow'" --once

    Lights can be read through ROS parameters (to be used in read-only mode).

        /<lightname>/state      # 0: off, 1: on
        /<lightname>/color      # { b: [0,1], g: [0,1], r: [0,1] }

    Examples:

        rosparam get /<lightname>/state
        0

        rosparam get /<lightname>/color
        { b: 0.0, g: 0.0, r: 1.0 }
        
        rosparam get /<lightname>/color/r
        1.0



