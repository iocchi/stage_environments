/*
 *  stageros
 *  Copyright (c) 2008, Willow Garage, Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**

@mainpage

@htmlinclude manifest.html
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>

// boost
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

// libstage
#include <stage.hh>

// Extended API for GUI control (requires custom Stage)
// NEW_GUI_ACCESS defined in stage.hh of custom Stage
#if NEW_GUI_ACCESS
#include <canvas.hh>
#endif

// roscpp
#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <rosgraph_msgs/Clock.h>
#include <std_msgs/String.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Float64.h>

#include "tf/transform_broadcaster.h"

#include "std_msgs/String.h"

#define USAGE "stageros <worldfile>"

// Topics
#define IMAGE "image"
#define DEPTH "depth"
#define CAMERA_INFO "camera_info"
#define ODOM "odom"
#define PEOPLE "/people"
#define LIGHTSTATE "state"
#define LIGHTCOLOR "color"

#define BASE_POSE_GROUND_TRUTH "base_pose_ground_truth"
#define CMD_VEL "cmd_vel"
#define LASER_TOPIC_DEFAULT  "scan"
#define CMD_SAY "stage_say"
#define SETPOSE "setpose"
#define STAGEGUIREQUEST_TOPIC "stageGUIRequest"

#define PANTILT_STATE "pantilt_state"
#define PANTILT_CMD "pantilt_cmd"
#define PAN_CMD "pan_controller/command"
#define TILT_CMD "tilt_controller/command"


// Parameters

#define PARAM_STALL "stalled"

// default values
#define BASE_FRAME_DEFAULT   "base_frame" 
#define BASE_FOOTPRINT_FRAME_DEFAULT   "base_footprint_frame" 
#define LASER_FRAME_DEFAULT  "laser_frame"
#define CAMERA_FRAME_DEFAULT "camera_frame"
#define ODOM_FRAME_DEFAULT   "odom"

// Model type ("robot", "person", ...)
std::map<Stg::Model *, std::string> modeltype; 



// Our node
class StageNode
{
  private:
    // Messages that we'll send or receive
    sensor_msgs::CameraInfo *cameraMsgs;
    sensor_msgs::Image *imageMsgs;
    sensor_msgs::Image *depthMsgs;
    sensor_msgs::LaserScan *laserMsgs;
    nav_msgs::Odometry *odomMsgs;
    nav_msgs::Odometry *groundTruthMsgs;
    rosgraph_msgs::Clock clockMsg;

    // roscpp-related bookkeeping
    ros::NodeHandle n_;

    // A mutex to lock access to fields that are used in message callbacks
    boost::mutex msg_lock;

    // The models that we're interested in
    std::vector<Stg::ModelCamera *> cameramodels;
    std::vector<Stg::ModelRanger *> lasermodels;
    std::vector<Stg::ModelPosition *> positionmodels;
    std::vector<Stg::Model *> personmodels;
    std::vector<Stg::ModelLightIndicator *> lightmodels; 

    std::vector<ros::Publisher> image_pubs_;
    std::vector<ros::Publisher> depth_pubs_;
    std::vector<ros::Publisher> camera_pubs_;
    std::vector<ros::Publisher> pantilt_pubs_;
    std::vector<ros::Publisher> laser_pubs_;
    std::vector<ros::Publisher> odom_pubs_;
    std::vector<ros::Publisher> ground_truth_pubs_;
    ros::Publisher clock_pub_;
    // People pub/sub
    ros::Publisher people_pub_;
    // Subscribers
    std::vector<ros::Subscriber> cmdvel_subs_;
    std::vector<ros::Subscriber> pantilt_subs_, pan_subs_, tilt_subs_;
    std::vector<ros::Subscriber> say_subs_;
    std::vector<ros::Subscriber> setpose_subs_;
    // Lights
    std::vector<ros::Subscriber> light_subs_; 
    // Receiving GUI requests  
    ros::Subscriber guirequest_sub;

    bool isDepthCanonical;
    bool use_model_names;
    
    std::string base_frame, base_footprint_frame, laser_topic, laser_frame, camera_frame, odom_frame;

    // A helper function that is executed for each stage model.  We use it
    // to search for models of interest.
    static void ghfunc(Stg::Model* mod, StageNode* node);

    static bool s_update(Stg::World* world, StageNode* node)
    {
      node->WorldCallback();
      // We return false to indicate that we want to be called again (an
      // odd convention, but that's the way that Stage works).
      return false;
    }

    // Appends the given robot ID to the given message name.  If omitRobotID
    // is true, an unaltered copy of the name is returned.    
    const char *mapName(const char *name, size_t robotID, Stg::Model* mod);
    const char *mapName(std::string sname, size_t robotID, Stg::Model* mod);


    tf::TransformBroadcaster tf;

    // Last time that we received a velocity command
    ros::Time base_last_cmd;
    ros::Duration base_watchdog_timeout;

    // Current simulation time
    ros::Time sim_time;
    
    // Last time we saved global position (for velocity calculation).
    ros::Time base_last_globalpos_time;
    // Last published global pose of each robot
    std::vector<Stg::Pose> base_last_globalpos;
    // Simulation speedup
    double speedup;

  public:
    // Constructor; stage itself needs argc/argv.  fname is the .world file
    // that stage should load.
    StageNode(int argc, char** argv, bool gui, const char* fname, bool use_model_names);
    ~StageNode();

    // Subscribe to models of interest.  Currently, we find and subscribe
    // to the first 'laser' model and the first 'position' model.  Returns
    // 0 on success (both models subscribed), -1 otherwise.
    int SubscribeModels();

    // Our callback
    void WorldCallback();
    
    // Do one update of the world.  May pause if the next update time
    // has not yet arrived.
    bool UpdateWorld();

    // Message callback for a MsgBaseVel message, which set velocities.
    void cmdvelReceived(int idx, const boost::shared_ptr<geometry_msgs::Twist const>& msg);

    // Message callback for pan-tilt control
    void cameraPanTilt_cb(int idx, const boost::shared_ptr<sensor_msgs::JointState const>& msg);
    void cameraPan_cb(int idx, const boost::shared_ptr<std_msgs::Float64 const>& msg);
    void cameraTilt_cb(int idx, const boost::shared_ptr<std_msgs::Float64 const>& msg);

    // Message callback for a String message, which tells something.
    void sayReceived(int idx, const boost::shared_ptr<std_msgs::String const>& msg);

    // Message callback for a set pose message, which set the global pose of the model
    void setposeReceived(int idx, const boost::shared_ptr<geometry_msgs::Pose const>& msg);

    // Message callback for lights messages
    void lightReceived(int idx, const boost::shared_ptr<std_msgs::Int8 const>& msg);
    void lightColorReceived(int idx, const boost::shared_ptr<std_msgs::String const>& msg);

    // Callback for GUI request messages
    void GUIRequest_cb(const boost::shared_ptr<std_msgs::String const>& msg);

    // Save a screenshot image of the GUI
    void Screenshot();

    // Show footprints in the GUI
    void Footprints(bool val);

    // Set simulation speedup
    void Speedup(double speedup);

    // Get requested simulation speed (for no-gui mode)
    double getSpeedup() { return speedup; }

    // The main simulator object
    Stg::World* world;
    Stg::WorldGui* worldgui;
};


const char *
StageNode::mapName(std::string sname, size_t robotID, Stg::Model* mod)
{
   return mapName(sname.c_str(), robotID, mod);
}


// since stageros is single-threaded, this is OK. revisit if that changes!
const char *
StageNode::mapName(const char *name, size_t robotID, Stg::Model* mod)
{
  bool umn = this->use_model_names;

  int nrobots = positionmodels.size() - personmodels.size();

  if ((nrobots > 1) || umn || (modeltype[mod] == "person")) {
    static char buf[100];
    std::size_t found = std::string(((Stg::Ancestor *) mod)->Token()).find(":");

    if (modeltype[mod] == "person") {
        snprintf(buf, sizeof(buf), "/%s/%s", ((Stg::Ancestor *) mod)->Token(), name);
    }
    else if ((found==std::string::npos) && umn) {
    	snprintf(buf, sizeof(buf), "/%s/%s", ((Stg::Ancestor *) mod)->Token(), name);
    }
    else {
 	    snprintf(buf, sizeof(buf), "/robot%u/%s", (unsigned int)robotID, name);
    }

    return buf;
  }
  else
    return name;
}


void
StageNode::ghfunc(Stg::Model* mod, StageNode* node)
{

  // std::cerr <<  "-- DEBUG -- " <<  mod->TokenStr() << std::endl;

  std::vector<Stg::Model*> cm = mod->GetChildren();

  //std::cerr <<  "-- MODEL -- " <<  mod->TokenStr() << std::endl;
  //std::cerr <<  "  -- CHILDREN --  " << std::endl;

  modeltype[mod] = "";

  std::vector<Stg::Model*>::iterator it = cm.begin();
  while (it < cm.end()) {
      // std::cerr <<  "    -- " <<  (*it)->TokenStr() << std::endl;
      if ((*it)->TokenStr()=="head")
        modeltype[mod] = "person";
      else if ((*it)->TokenStr()=="robot")
        modeltype[mod] = "robot";
      it++;
  }

  if (dynamic_cast<Stg::ModelRanger *>(mod)) {
    node->lasermodels.push_back(dynamic_cast<Stg::ModelRanger *>(mod));
  }
  else if (dynamic_cast<Stg::ModelPosition *>(mod)) {
    node->positionmodels.push_back(dynamic_cast<Stg::ModelPosition *>(mod));
    // Check if person
    if (modeltype[mod] == "person")
      node->personmodels.push_back(mod);
  }
  else if (dynamic_cast<Stg::ModelCamera *>(mod)) {
    node->cameramodels.push_back(dynamic_cast<Stg::ModelCamera *>(mod));
  }
  else if (modeltype[mod] == "person") {
    node->personmodels.push_back(mod);
  }
  else if (dynamic_cast<Stg::ModelLightIndicator *>(mod)) {
     // std::cerr <<  "-- DEBUG -- light " <<  mod->TokenStr() << std::endl;
     node->lightmodels.push_back(dynamic_cast<Stg::ModelLightIndicator *>(mod));
  }

}

// ROS Callback for cmd_vel
void
StageNode::cmdvelReceived(int idx, const boost::shared_ptr<geometry_msgs::Twist const>& msg)
{
  boost::mutex::scoped_lock lock(msg_lock);
  this->positionmodels[idx]->SetSpeed(msg->linear.x, 
                                      msg->linear.y, 
                                      msg->angular.z);
  this->base_last_cmd = this->sim_time;
}

// ROS Callback for camera pan-tilt control
// using sensor_msgs/JointStage msg
// Header header
//
// string[] name
// float64[] position  
// float64[] velocity
// float64[] effort
void StageNode::cameraPanTilt_cb(int idx, const boost::shared_ptr<sensor_msgs::JointState const>& msg)
{
  this->cameramodels[idx]->setYaw(-msg->position[0]);  // degrees
  this->cameramodels[idx]->setPitch(-msg->position[1]);  // degrees
  //this->cameramodels[idx]->getCamera().Draw();
}

void StageNode::cameraPan_cb(int idx, const boost::shared_ptr<std_msgs::Float64 const>& msg) {
  double d = -Stg::rtod(msg->data);
  this->cameramodels[idx]->setYaw(d);  // degrees
}


void StageNode::cameraTilt_cb(int idx, const boost::shared_ptr<std_msgs::Float64 const>& msg) {
  double d = -Stg::rtod(msg->data);
  this->cameramodels[idx]->setPitch(d);  // degrees
}


// ROS Callback for say
void StageNode::sayReceived(int idx, const boost::shared_ptr<std_msgs::String const>& msg)
{
  this->positionmodels[idx]->Say(msg->data);
}

// ROS Callback for set pose
void StageNode::setposeReceived(int idx, const boost::shared_ptr<geometry_msgs::Pose const>& msg)
{
  ROS_INFO_STREAM("setpose " << *msg);
  Stg::Pose gpose;
  gpose.x = msg->position.x;
  gpose.y = msg->position.y;
  gpose.z = msg->position.z;

  tf::Quaternion q(
    msg->orientation.x,
    msg->orientation.y,
    msg->orientation.z,
    msg->orientation.w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  gpose.a = yaw;

  this->positionmodels[idx]->SetGlobalPose(gpose);	
}

// ROS Callback for light control
void StageNode::lightReceived(int idx, const boost::shared_ptr<std_msgs::Int8 const>& msg)
{
    boost::mutex::scoped_lock lock(msg_lock);
    this->lightmodels[idx]->SetState(msg->data);
    char buf[32];
    snprintf(buf, sizeof(buf), "/%s/%s", this->lightmodels[idx]->Token(), LIGHTSTATE);
    n_.setParam(buf, msg->data);
}

// ROS Callback for light colors
void StageNode::lightColorReceived(int idx, const boost::shared_ptr<std_msgs::String const>& msg)
{
    boost::mutex::scoped_lock lock(msg_lock);
    this->lightmodels[idx]->ChangeColor(msg->data);

    Stg::Color c = this->lightmodels[idx]->GetColor();
    std::map<std::string,double> map_color;
    map_color["r"] = c.r;
    map_color["g"] = c.g;
    map_color["b"] = c.b;

    char buf[32];
    snprintf(buf, sizeof(buf), "/%s/%s", this->lightmodels[idx]->Token(), LIGHTCOLOR);
    n_.setParam(buf, map_color);

}

// ROS Callback for GUI requests
void StageNode::GUIRequest_cb(const boost::shared_ptr<std_msgs::String const>& msg)
{
    ROS_INFO_STREAM(msg->data);
    if (msg->data=="screenshot")
        Screenshot();
    else if (msg->data=="footprints")
        Footprints(true);
    else if (msg->data=="footprints_off")
        Footprints(false);
    else if (msg->data=="pause")
        this->world->Stop();
    else if (msg->data=="resume")
        this->world->Start();
    else if (msg->data.substr(0,7)=="speedup") {
        std::vector<std::string> pp;
        boost::split(pp, msg->data, boost::is_any_of("_"));
        float s=1.0;
        if (pp.size()>1) {
          s = atof(pp[1].c_str());
          if (s<1e-6) s=1.0;
        }
        Speedup(s);
    }
}




StageNode::StageNode(int argc, char** argv, bool gui, const char* fname, bool use_model_names)
{
  this->use_model_names = use_model_names;
  this->sim_time.fromSec(0.0);
  this->base_last_cmd.fromSec(0.0);
  double t;
  ros::NodeHandle localn("~");
  if(!localn.getParam("base_watchdog_timeout", t))
    t = 0.5;
  this->base_watchdog_timeout.fromSec(t);
  this->speedup = 1.0;

  this->people_pub_ = n_.advertise<geometry_msgs::PoseStamped>(PEOPLE, 10);

  if(!localn.getParam("is_depth_canonical", isDepthCanonical))
    isDepthCanonical = true;
  
  if(!localn.getParam("base_frame", this->base_frame))
    this->base_frame = BASE_FRAME_DEFAULT;
  if(!localn.getParam("base_footprint_frame", this->base_footprint_frame))
    this->base_footprint_frame = BASE_FOOTPRINT_FRAME_DEFAULT;
  if(!localn.getParam("laser_topic", this->laser_topic))
    this->laser_topic = LASER_TOPIC_DEFAULT;
  if(!localn.getParam("laser_frame", this->laser_frame))
    this->laser_frame = LASER_FRAME_DEFAULT;
  if(!localn.getParam("camera_frame", this->camera_frame))
    this->camera_frame = CAMERA_FRAME_DEFAULT;
  if(!localn.getParam("odom_frame", this->odom_frame))
    this->odom_frame = ODOM_FRAME_DEFAULT;


  // We'll check the existence of the world file, because libstage doesn't
  // expose its failure to open it.  Could go further with checks (e.g., is
  // it readable by this user).
  struct stat s;
  if(stat(fname, &s) != 0)
  {
    ROS_FATAL("The world file %s does not exist.", fname);
    ROS_BREAK();
  }

  // initialize libstage
  Stg::Init( &argc, &argv );

  if (gui) {
    this->worldgui = new Stg::WorldGui(600, 400, "Stage (ROS)");
    this->world = this->worldgui;
  }
  else {
    this->world = new Stg::World();
    this->worldgui = NULL;
  }


  // Apparently an Update is needed before the Load to avoid crashes on
  // startup on some systems.
  // As of Stage 4.1.1, this update call causes a hang on start.
  //this->UpdateWorld();
  this->world->Load(fname);

  // We add our callback here, after the Update, so avoid our callback
  // being invoked before we're ready.
  this->world->AddUpdateCallback((Stg::world_callback_t)s_update, this);

  this->world->ForEachDescendant((Stg::model_callback_t)ghfunc, this);
  if (lasermodels.size() > 0 && lasermodels.size() != positionmodels.size())
  {
    // ROS_WARN("number of position models and laser models must be equal in "
    //          "the world file.");
    // ROS_BREAK();
  }
  else if (cameramodels.size() > 0 && cameramodels.size() != positionmodels.size())
  {
    // ROS_WARN("number of position models and camera models must be equal in "
    //          "the world file.");
    // ROS_BREAK();
  }
  size_t numRobots = positionmodels.size();
  ROS_INFO("found %u position and laser(%u)/camera(%u) pair%s in the file", 
           (unsigned int)numRobots, (unsigned int) lasermodels.size(), (unsigned int) cameramodels.size(), (numRobots==1) ? "" : "s");

  this->laserMsgs = new sensor_msgs::LaserScan[numRobots];
  this->odomMsgs = new nav_msgs::Odometry[numRobots];
  this->groundTruthMsgs = new nav_msgs::Odometry[numRobots];
  this->imageMsgs = new sensor_msgs::Image[numRobots];
  this->depthMsgs = new sensor_msgs::Image[numRobots];
  this->cameraMsgs = new sensor_msgs::CameraInfo[numRobots];
}


// Subscribe to models of interest.  Currently, we find and subscribe
// to the first 'laser' model and the first 'position' model.  Returns
// 0 on success (both models subscribed), -1 otherwise.
//
// Eventually, we should provide a general way to map stage models onto ROS
// topics, similar to Player .cfg files.
int
StageNode::SubscribeModels()
{
  n_.setParam("/use_sim_time", true);
/*
  for (size_t r = 0; r < this->personmodels.size(); r++) {
    ROS_INFO("-- Subscribing to person %d %s", r, this->personmodels[r]->Token());
    this->personmodels[r]->Subscribe();
  }
*/

  for (size_t r = 0; r < this->positionmodels.size(); r++)
  {
    Stg::Model * mod = this->positionmodels[r];
    std::string type = modeltype[mod];
    ROS_INFO("-- Subscribing to %s %lu %s", type.c_str(), r, mod->Token());

    if(this->lasermodels.size()>r && this->lasermodels[r]) {
      this->lasermodels[r]->Subscribe();
    }
    else if (this->lasermodels.size()>0) {
      // ROS_WARN_STREAM("no laser for model " << this->cameramodels.size());
    }

    if (mod) {
      mod->Subscribe();
    }
    else {
      ROS_ERROR("no position");
      ROS_BREAK();
    }

    if (this->cameramodels.size()>r && this->cameramodels[r]) {
      this->cameramodels[r]->Subscribe();
    }
    else if (this->cameramodels.size()>0) {
      // ROS_WARN_STREAM("no camera for model " << this->cameramodels.size());
    }

    if (this->lightmodels.size()>r && this->lightmodels[r]) {
      this->lightmodels[r]->Subscribe();
    }
    else if (this->lightmodels.size()>0) {
      // TODO ???
      // ROS_WARN_STREAM("no light for robot " << this->lightmodels.size());
    }


    if (this->lasermodels.size()>r) {
      laser_pubs_.push_back(n_.advertise<sensor_msgs::LaserScan>(
        mapName(this->laser_topic,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
    }

    odom_pubs_.push_back(n_.advertise<nav_msgs::Odometry>(
        mapName(ODOM,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
        
    ground_truth_pubs_.push_back(n_.advertise<nav_msgs::Odometry>(
        mapName(BASE_POSE_GROUND_TRUTH,r,static_cast<Stg::Model*>(positionmodels[r])), 10));

    if (this->cameramodels.size()>r && this->cameramodels[r]) {
      image_pubs_.push_back(n_.advertise<sensor_msgs::Image>(
        mapName(IMAGE,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
      depth_pubs_.push_back(n_.advertise<sensor_msgs::Image>(
        mapName(DEPTH,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
      camera_pubs_.push_back(n_.advertise<sensor_msgs::CameraInfo>(
        mapName(CAMERA_INFO,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
      pantilt_subs_.push_back(n_.subscribe<sensor_msgs::JointState>(
        mapName(PANTILT_CMD,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::cameraPanTilt_cb, this, r, _1)));
      pan_subs_.push_back(n_.subscribe<std_msgs::Float64>(
        mapName(PAN_CMD,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::cameraPan_cb, this, r, _1)));
      tilt_subs_.push_back(n_.subscribe<std_msgs::Float64>(
        mapName(TILT_CMD,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::cameraTilt_cb, this, r, _1)));
      pantilt_pubs_.push_back(n_.advertise<sensor_msgs::JointState>(
        mapName(PANTILT_STATE,r,static_cast<Stg::Model*>(positionmodels[r])), 10));
      //ROS_INFO("-- pantilt cb for topic %s ",
      //  mapName(PANTILT_CMD,r,static_cast<Stg::Model*>(positionmodels[r])));

    }

    cmdvel_subs_.push_back(n_.subscribe<geometry_msgs::Twist>(
        mapName(CMD_VEL,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::cmdvelReceived, this, r, _1)));

    say_subs_.push_back(n_.subscribe<std_msgs::String>(
        mapName(CMD_SAY,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::sayReceived, this, r, _1)));

    setpose_subs_.push_back(n_.subscribe<geometry_msgs::Pose>(
        mapName(SETPOSE,r,static_cast<Stg::Model*>(positionmodels[r])), 10,
        boost::bind(&StageNode::setposeReceived, this, r, _1)));

    /* robot lights
    if (this->lightmodels.size()>r) {
        light_subs_.push_back(n_.subscribe<std_msgs::Int8>(
            mapName(LIGHTSTATE, r, static_cast<Stg::Model*>(positionmodels[r])), 10,
            boost::bind(&StageNode::lightReceived, this, r, _1)));

        light_subs_.push_back(n_.subscribe<std_msgs::String>(
            mapName(LIGHTCOLOR, r, static_cast<Stg::Model*>(positionmodels[r])), 10,
            boost::bind(&StageNode::lightColorReceived, this, r, _1)));
    }
    */

  } // for each position model


  // all lights
  for (size_t r = 0; r < this->lightmodels.size(); r++) {

    const char* ls = this->lightmodels[r]->Token();

    static char buf[32];
    snprintf(buf, sizeof(buf), "/%s/%s", ls, LIGHTSTATE);

    // state
    this->lightmodels[r]->SetIntensity(0.5);
    this->lightmodels[r]->SetState(0);
    n_.setParam(buf, 0);

    light_subs_.push_back(n_.subscribe<std_msgs::Int8>(
        buf, 10,
        boost::bind(&StageNode::lightReceived, this, r, _1)));

    // color
    snprintf(buf, sizeof(buf), "/%s/%s", ls, LIGHTCOLOR);

    light_subs_.push_back(n_.subscribe<std_msgs::String>(
        buf, 10,
        boost::bind(&StageNode::lightColorReceived, this, r, _1)));

    Stg::Color c = this->lightmodels[r]->GetColor();

    std::map<std::string,double> map_color;
    map_color["r"] = c.r;
    map_color["g"] = c.g;
    map_color["b"] = c.b;

    snprintf(buf, sizeof(buf), "/%s/%s", ls, LIGHTCOLOR);

    n_.setParam(buf, map_color);

  }
    

  clock_pub_ = n_.advertise<rosgraph_msgs::Clock>("/clock",10);

  guirequest_sub = n_.subscribe<std_msgs::String>(STAGEGUIREQUEST_TOPIC,10,
        boost::bind(&StageNode::GUIRequest_cb, this, _1));

  return 0;

}

StageNode::~StageNode()
{
  delete[] laserMsgs;
  delete[] odomMsgs;
  delete[] groundTruthMsgs;
  delete[] imageMsgs;
  delete[] depthMsgs;
  delete[] cameraMsgs;

  n_.setParam("/use_sim_time", false);
}

bool
StageNode::UpdateWorld()
{
  if (this->world->Paused())
    return false;
  else
    return this->world->UpdateAll();
}

void
StageNode::WorldCallback()
{
  boost::mutex::scoped_lock lock(msg_lock);

  this->sim_time.fromSec(world->SimTimeNow() / 1e6);
  // We're not allowed to publish clock==0, because it used as a special
  // value in parts of ROS, #4027.
  if(this->sim_time.sec == 0 && this->sim_time.nsec == 0)
  {
    ROS_DEBUG("Skipping initial simulation step, to avoid publishing clock==0");
    return;
  }

  // TODO make this only affect one robot if necessary
  if((this->base_watchdog_timeout.toSec() > 0.0) &&
      ((this->sim_time - this->base_last_cmd) >= this->base_watchdog_timeout))
  {
    for (size_t r = 0; r < this->positionmodels.size(); r++)
      this->positionmodels[r]->SetSpeed(0.0, 0.0, 0.0);
  }

  // Get people data
  for (size_t r = 0; r < this->personmodels.size(); r++) {
    geometry_msgs::PoseStamped msg;
    msg.pose.position.x = this->personmodels[r]->GetGlobalPose().x;
    msg.pose.position.y = this->personmodels[r]->GetGlobalPose().y;
    msg.pose.orientation = tf::createQuaternionMsgFromYaw(this->personmodels[r]->GetGlobalPose().a);
    msg.header.frame_id = ("/"+this->personmodels[r]->TokenStr()).c_str();
    msg.header.stamp = sim_time;
    this->people_pub_.publish(msg);
  }

  // Get latest laser data
  for (size_t r = 0; r < this->lasermodels.size(); r++)	{
    const std::vector<Stg::ModelRanger::Sensor>& sensors = this->lasermodels[r]->GetSensors();
		
	if ( sensors.size() > 1 )
		ROS_WARN( "ROS Stage currently supports rangers with 1 sensor only." );

	// for now we access only the zeroth sensor of the ranger - good
	// enough for most laser models that have a single beam origin
	const Stg::ModelRanger::Sensor& s = sensors[0];
		
    if( s.ranges.size() ) {
      // Translate into ROS message format and publish
      this->laserMsgs[r].angle_min = -s.fov/2.0;
      this->laserMsgs[r].angle_max = +s.fov/2.0;
      this->laserMsgs[r].angle_increment = s.fov/(double)(s.sample_count-1);
      this->laserMsgs[r].range_min = s.range.min;
      this->laserMsgs[r].range_max = s.range.max;
      this->laserMsgs[r].ranges.resize(s.ranges.size());
      this->laserMsgs[r].intensities.resize(s.intensities.size());
			
      for(unsigned int i=0; i<s.ranges.size(); i++) {
		this->laserMsgs[r].ranges[i] = s.ranges[i];
		this->laserMsgs[r].intensities[i] = (uint8_t)s.intensities[i];
	  }

      this->laserMsgs[r].header.frame_id = mapName(this->laser_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
      this->laserMsgs[r].header.stamp = sim_time;
      this->laser_pubs_[r].publish(this->laserMsgs[r]);
    }

    // Also publish the base->laser_frame Tx.  This could eventually move
    // into being retrieved from the param server as a static Tx.
    Stg::Pose lp = this->lasermodels[r]->GetPose();
    tf::Quaternion laserQ;
    laserQ.setRPY(0.0, 0.0, lp.a);
    tf::Transform txLaser =  tf::Transform(laserQ,
                                            tf::Point(lp.x, lp.y, this->positionmodels[r]->GetGeom().size.z+lp.z));
    tf.sendTransform(tf::StampedTransform(txLaser, sim_time,
                                          mapName(this->base_frame, r,static_cast<Stg::Model*>(positionmodels[r])),
                                          mapName(this->laser_frame, r,static_cast<Stg::Model*>(positionmodels[r]))));
    }
    
    for (size_t r = 0; r < this->positionmodels.size(); r++) {
        // Send the identity transform between base_footprint and base_frame
        tf::Transform txIdentity(tf::createIdentityQuaternion(),
                                 tf::Point(0, 0, 0));
        tf.sendTransform(tf::StampedTransform(txIdentity, sim_time,
              mapName(this->base_footprint_frame, r,static_cast<Stg::Model*>(positionmodels[r])),
              mapName(this->base_frame, r,static_cast<Stg::Model*>(positionmodels[r]))));

        // Get latest odometry data
        // Translate into ROS message format and publish
        this->odomMsgs[r].pose.pose.position.x = this->positionmodels[r]->est_pose.x;
        this->odomMsgs[r].pose.pose.position.y = this->positionmodels[r]->est_pose.y;
        this->odomMsgs[r].pose.pose.orientation = tf::createQuaternionMsgFromYaw(this->positionmodels[r]->est_pose.a);
        Stg::Velocity v = this->positionmodels[r]->GetVelocity();
        this->odomMsgs[r].twist.twist.linear.x = v.x;
        this->odomMsgs[r].twist.twist.linear.y = v.y;
        this->odomMsgs[r].twist.twist.angular.z = v.a;

        // publish odom messages
        this->odomMsgs[r].header.frame_id = mapName(this->odom_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
        this->odomMsgs[r].header.stamp = sim_time;

        this->odom_pubs_[r].publish(this->odomMsgs[r]);

        // broadcast odometry transform
        tf::Quaternion odomQ;
        tf::quaternionMsgToTF(odomMsgs[r].pose.pose.orientation, odomQ);
        tf::Transform txOdom(odomQ, 
                         tf::Point(odomMsgs[r].pose.pose.position.x,
                                   odomMsgs[r].pose.pose.position.y, 0.0));
        tf.sendTransform(tf::StampedTransform(txOdom, sim_time,
                  mapName(this->odom_frame, r,static_cast<Stg::Model*>(positionmodels[r])),
                  mapName(this->base_footprint_frame, r,static_cast<Stg::Model*>(positionmodels[r]))));

        // Also publish the ground truth pose and velocity
        Stg::Pose gpose = this->positionmodels[r]->GetGlobalPose();
        tf::Quaternion q_gpose;
        q_gpose.setRPY(0.0, 0.0, gpose.a);
        tf::Transform gt(q_gpose, tf::Point(gpose.x, gpose.y, 0.0));
        // Velocity is 0 by default and will be set only if there is previous pose and time delta>0
        Stg::Velocity gvel(0,0,0,0);
        if (this->base_last_globalpos.size()>r) {
          Stg::Pose prevpose = this->base_last_globalpos.at(r);
          double dT = (this->sim_time-this->base_last_globalpos_time).toSec();
          if (dT>0)
            gvel = Stg::Velocity(
              (gpose.x - prevpose.x)/dT, 
              (gpose.y - prevpose.y)/dT, 
              (gpose.z - prevpose.z)/dT, 
              Stg::normalize(gpose.a - prevpose.a)/dT
            );
          this->base_last_globalpos.at(r) = gpose;
        } 
        else //There are no previous readings, adding current pose...
          this->base_last_globalpos.push_back(gpose);

        this->groundTruthMsgs[r].pose.pose.position.x     = gt.getOrigin().x();
        this->groundTruthMsgs[r].pose.pose.position.y     = gt.getOrigin().y();
        this->groundTruthMsgs[r].pose.pose.position.z     = gt.getOrigin().z();
        this->groundTruthMsgs[r].pose.pose.orientation.x  = gt.getRotation().x();
        this->groundTruthMsgs[r].pose.pose.orientation.y  = gt.getRotation().y();
        this->groundTruthMsgs[r].pose.pose.orientation.z  = gt.getRotation().z();
        this->groundTruthMsgs[r].pose.pose.orientation.w  = gt.getRotation().w();
        this->groundTruthMsgs[r].twist.twist.linear.x = gvel.x;
        this->groundTruthMsgs[r].twist.twist.linear.y = gvel.y;
        this->groundTruthMsgs[r].twist.twist.linear.z = gvel.z;
        this->groundTruthMsgs[r].twist.twist.angular.z = gvel.a;

        this->groundTruthMsgs[r].header.frame_id = mapName(this->odom_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
        this->groundTruthMsgs[r].header.stamp = sim_time;

        this->ground_truth_pubs_[r].publish(this->groundTruthMsgs[r]);

        // Set stall parameter for each position model
        bool stall = this->positionmodels[r]->Stalled(); // model crashed into another model
        n_.setParam(mapName(PARAM_STALL,r,static_cast<Stg::Model*>(positionmodels[r])), stall);

  }
  
  this->base_last_globalpos_time = this->sim_time;
  
  for (size_t r = 0; r < this->cameramodels.size(); r++)
  {
    // Get latest image data
    // Translate into ROS message format and publish
    if (this->image_pubs_[r].getNumSubscribers()>0 && this->cameramodels[r]->FrameColor()) {
       this->imageMsgs[r].height=this->cameramodels[r]->getHeight();
       this->imageMsgs[r].width=this->cameramodels[r]->getWidth();
       this->imageMsgs[r].encoding="rgba8";
       //this->imageMsgs[r].is_bigendian="";
       this->imageMsgs[r].step=this->imageMsgs[r].width*4;
       this->imageMsgs[r].data.resize(this->imageMsgs[r].width*this->imageMsgs[r].height*4);

       memcpy(&(this->imageMsgs[r].data[0]),this->cameramodels[r]->FrameColor(),this->imageMsgs[r].width*this->imageMsgs[r].height*4);

       //invert the opengl weirdness
       int height = this->imageMsgs[r].height - 1;
       int linewidth = this->imageMsgs[r].width*4;

       char* temp = new char[linewidth];
       for (int y = 0; y < (height+1)/2; y++) 
       {
            memcpy(temp,&this->imageMsgs[r].data[y*linewidth],linewidth);
            memcpy(&(this->imageMsgs[r].data[y*linewidth]),&(this->imageMsgs[r].data[(height-y)*linewidth]),linewidth);
            memcpy(&(this->imageMsgs[r].data[(height-y)*linewidth]),temp,linewidth);
       }
       delete[] temp;

        this->imageMsgs[r].header.frame_id = mapName(this->camera_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
        this->imageMsgs[r].header.stamp = sim_time;

        this->image_pubs_[r].publish(this->imageMsgs[r]);      
    }
    //Get latest depth data
    //Translate into ROS message format and publish
    //Skip if there are no subscribers
    if (this->depth_pubs_[r].getNumSubscribers()>0 && this->cameramodels[r]->FrameDepth()) {
      this->depthMsgs[r].height=this->cameramodels[r]->getHeight();
      this->depthMsgs[r].width=this->cameramodels[r]->getWidth();
      this->depthMsgs[r].encoding=this->isDepthCanonical?sensor_msgs::image_encodings::TYPE_32FC1:sensor_msgs::image_encodings::TYPE_16UC1;
      //this->depthMsgs[r].is_bigendian="";
      int sz = this->isDepthCanonical?sizeof(float):sizeof(uint16_t);
      size_t len = this->depthMsgs[r].width*this->depthMsgs[r].height;
      this->depthMsgs[r].step=this->depthMsgs[r].width*sz;
      this->depthMsgs[r].data.resize(len*sz);

      //processing data according to REP118
      if (this->isDepthCanonical){
        double nearClip = this->cameramodels[r]->getCamera().nearClip();
        double farClip = this->cameramodels[r]->getCamera().farClip();
        memcpy(&(this->depthMsgs[r].data[0]),this->cameramodels[r]->FrameDepth(),len*sz);
        float * data = (float*)&(this->depthMsgs[r].data[0]);
        for (size_t i=0;i<len;++i)
          if(data[i]<=nearClip) 
            data[i] = -INFINITY;
          else if(data[i]>=farClip) 
            data[i] = INFINITY;
      }
      else{
        int nearClip = (int)(this->cameramodels[r]->getCamera().nearClip() * 1000);
        int farClip = (int)(this->cameramodels[r]->getCamera().farClip() * 1000);
        for (size_t i=0;i<len;++i){
          int v = (int)(this->cameramodels[r]->FrameDepth()[i]*1000);
          if (v<=nearClip || v>=farClip) v = 0;
          ((uint16_t*)&(this->depthMsgs[r].data[0]))[i] = (uint16_t) ((v<=nearClip || v>=farClip) ? 0 : v );
        }
      }
      
      //invert the opengl weirdness
      int height = this->depthMsgs[r].height - 1;
      int linewidth = this->depthMsgs[r].width*sz;

      char* temp = new char[linewidth];
      for (int y = 0; y < (height+1)/2; y++) 
      {
           memcpy(temp,&this->depthMsgs[r].data[y*linewidth],linewidth);
           memcpy(&(this->depthMsgs[r].data[y*linewidth]),&(this->depthMsgs[r].data[(height-y)*linewidth]),linewidth);
           memcpy(&(this->depthMsgs[r].data[(height-y)*linewidth]),temp,linewidth);
      }
      delete[] temp;

      this->depthMsgs[r].header.frame_id = mapName(this->camera_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
      this->depthMsgs[r].header.stamp = sim_time;
      this->depth_pubs_[r].publish(this->depthMsgs[r]);
    }
    
    //sending camera's tf and info only if image or depth topics are subscribed to
    if ((this->image_pubs_[r].getNumSubscribers()>0 && this->cameramodels[r]->FrameColor())
    || (this->depth_pubs_[r].getNumSubscribers()>0 && this->cameramodels[r]->FrameDepth()))
    {
       
      Stg::Pose lp = this->cameramodels[r]->GetPose();
      tf::Quaternion Q; Q.setRPY(
        (this->cameramodels[r]->getCamera().pitch()*M_PI/180.0)-M_PI, 
        0.0, 
        lp.a+(this->cameramodels[r]->getCamera().yaw()*M_PI/180.0)-this->positionmodels[r]->GetPose().a
        );
        
      tf::Transform tr =  tf::Transform(Q, tf::Point(lp.x, lp.y, this->positionmodels[r]->GetGeom().size.z+lp.z));
      tf.sendTransform(tf::StampedTransform(tr, sim_time,
                                          mapName(this->base_frame, r,static_cast<Stg::Model*>(positionmodels[r])),
                                          mapName(this->camera_frame, r,static_cast<Stg::Model*>(positionmodels[r]))));
      
      this->cameraMsgs[r].header.frame_id = mapName(this->camera_frame, r,static_cast<Stg::Model*>(positionmodels[r]));
      this->cameraMsgs[r].header.stamp = sim_time;
      this->cameraMsgs[r].height = this->cameramodels[r]->getHeight();
      this->cameraMsgs[r].width = this->cameramodels[r]->getWidth();
      
      double fx,fy,cx,cy;
      cx = this->cameraMsgs[r].width / 2.0;
      cy = this->cameraMsgs[r].height / 2.0;
      double fovh = this->cameramodels[r]->getCamera().horizFov()*M_PI/180.0;
      double fovv = this->cameramodels[r]->getCamera().vertFov()*M_PI/180.0;
      //double fx_ = 1.43266615300557*this->cameramodels[r]->getWidth()/tan(fovh);
      //double fy_ = 1.43266615300557*this->cameramodels[r]->getHeight()/tan(fovv);
      fx = this->cameramodels[r]->getWidth()/(2*tan(fovh/2));
      fy = this->cameramodels[r]->getHeight()/(2*tan(fovv/2));
      
      //ROS_INFO("fx=%.4f,%.4f; fy=%.4f,%.4f", fx, fx_, fy, fy_);
 
      
      this->cameraMsgs[r].D.resize(4, 0.0);

      this->cameraMsgs[r].K[0] = fx;
      this->cameraMsgs[r].K[2] = cx;
      this->cameraMsgs[r].K[4] = fy;
      this->cameraMsgs[r].K[5] = cy;
      this->cameraMsgs[r].K[8] = 1.0;

      this->cameraMsgs[r].R[0] = 1.0;
      this->cameraMsgs[r].R[4] = 1.0;
      this->cameraMsgs[r].R[8] = 1.0;

      this->cameraMsgs[r].P[0] = fx;
      this->cameraMsgs[r].P[2] = cx;
      this->cameraMsgs[r].P[5] = fy;
      this->cameraMsgs[r].P[6] = cy;
      this->cameraMsgs[r].P[10] = 1.0;
      
      this->camera_pubs_[r].publish(this->cameraMsgs[r]);

    } // if subscribed to camera

    if (this->cameramodels[r]) {
        // publish pan-tilt status
        sensor_msgs::JointState js;
        js.header.stamp = sim_time; 
        js.name.resize(2);
        js.position.resize(2);
        js.name[0] = "yaw";
        js.name[1] = "pitch";

        double a = this->cameramodels[r]->GetGlobalPose().a;
        // this is needed because Stage makes the inverse conversion !!!
        js.position[0]= this->cameramodels[r]->getCamera().yaw() + 90 - Stg::rtod(a);
        js.position[1]= this->cameramodels[r]->getCamera().pitch() - 90;
        this->pantilt_pubs_[r].publish(js);
    }

  } // for cameras

  this->clockMsg.clock = sim_time;
  this->clock_pub_.publish(this->clockMsg);
}


#if NEW_GUI_ACCESS
void
StageNode::Speedup(double speedup)
{
  ROS_INFO("Setting simulation speedup = %.1f",speedup);
  this->speedup = speedup;
  if (this->worldgui) {
    this->worldgui->setSpeedup(speedup);    
  }
}

void 
StageNode::Footprints(bool val)
{
  if (this->worldgui) {
    Stg::Canvas* c = this->worldgui->GetCanvas();
    c->showFootprints.set(val);
  }
}

void 
StageNode::Screenshot()
{
  if (this->worldgui) {
    Stg::Canvas* c = this->worldgui->GetCanvas();

    c->showScreenshots.set(true);
    ros::Duration(2.0).sleep();
    c->showScreenshots.set(false);
  }
}
#else
void
StageNode::Speedup(double speedup)
{
  ROS_INFO("Setting simulation speedup = %.1f",speedup);
  this->speedup = speedup;
}

void 
StageNode::Footprints(bool val)
{ }

void 
StageNode::Screenshot()
{ }
#endif


int 
main(int argc, char** argv)
{ 
  if( argc < 2 )
  {
    puts(USAGE);
    exit(-1);
  }

  ros::init(argc, argv, "stageros");

  bool gui = true;
  bool use_model_names = false;
  for(int i=0;i<(argc-1);i++)
  {
    if(!strcmp(argv[i], "-g"))
      gui = false;
    if(!strcmp(argv[i], "-u"))
      use_model_names = true;
  }

  StageNode sn(argc-1,argv,gui,argv[argc-1], use_model_names);

  if(sn.SubscribeModels() != 0)
    exit(-1);

  boost::thread t = boost::thread(boost::bind(&ros::spin));

  // New in Stage 4.1.1: must Start() the world.
  sn.world->Start();

  // TODO: get rid of this fixed-duration sleep, using some Stage builtin
  // PauseUntilNextUpdate() functionality.
  ros::WallRate r(10.0);
  while(ros::ok() && !sn.world->TestQuit())
  {
    if(gui)
      Fl::wait(r.expectedCycleTime().toSec());
    else
    {
      sn.UpdateWorld();
      // cycle = sim_interval / speedup
      double sim_interval = 0.1;
#if NEW_GUI_ACCESS
      sim_interval = sn.world->getSimInterval()/1e6;
#endif
      double tm = sim_interval / sn.getSpeedup();
      ros::WallDuration d(tm);
      d.sleep();
    }
  }
  t.join();

  exit(0);
}

