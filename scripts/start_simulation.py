#!/usr/bin/env python

from __future__ import print_function

import tkMessageBox
import ConfigParser
import thread
import socket

from ttk import *
try:
  import Tkinter as tk
  from Tkinter import *
  #import Image, tkFileDialog
except:
  print('Import Error')
  print('Install with sudo apt-get install python-imaging-tk')
import argparse

import numpy as np
import sys, time, os, glob, shutil, math, datetime

import readmaps

# globals
run_gradient_based_navigation = False

Robots = [ 'orazio', 'marrtino', 'diago', 'sapienzbot' ]

Nrobots = ['1','2','3','4']

Localization = ['none', 'amcl', 'thin_localizer', 'srrg_localizer', 'gmapping', 'cg_mrslam', 'srrg_mapper2d']

Navigation = ['none','gradient_based_nav','move_base','move_base+grad_nav','spqrel_planner','spqrel_planner+grad_nav','explorer']

Demo = ['none', 'hri_pnp', 'rcathome','saman2']

LASER_TOPIC = 'scan'
LASER_TOPIC_STAGE = 'scan' # for using laser_filters
LASER_FRAME = 'laser_frame'

default_port = 9235

use_tmux = False 

def send_command(cmd, name):
    global use_tmux

    print(cmd)

    if use_tmux:
        sessionname = 'sim'
        r = os.system('tmux select-window -t %s:0' %sessionname)
        if r!=0:
            os.system('tmux -2 new-session -d -s %s' %sessionname)
        r = os.system('tmux select-window -t %s' %name)
        if (r!=0): # if not existing
            os.system('tmux new-window -n %s' %name)
        os.system('tmux send-keys "%s" C-m' %cmd)
    else:
        os.system('xterm -hold -e "%s" &' %cmd)


def start_robot(robot_name, robot_id, nrobots, mapdir, map_name, localization, navigation, INITPOSE_X,INITPOSE_Y,INITPOSE_TH_RAD, demo):

  global run_gradient_based_navigation

  no_localizer=False
  run_amcl=False
  run_thin_localizer=False
  run_srrg_localizer=False
  run_gmapping=False
  run_cg_mrslam=False
  run_srrg_mapper2d=False
  run_gradient_based_navigation=False
  run_move_base=False
  run_move_base_grad_nav=False
  run_thin_planner=False
  run_spqrel_planner=False
  run_spqrel_planner_grad_nav=False
  run_explorer=False

  if (localization=='amcl'):
    run_amcl=True
  elif (localization=='thin_localizer'):
    run_thin_localizer=True
  elif (localization=='srrg_localizer'):
    run_srrg_localizer=True
  elif (localization=='gmapping'):
    run_gmapping=True
  elif (localization=='cg_mrslam'):
    run_cg_mrslam=True
  elif (localization=='srrg_mapper2d'):
    run_srrg_mapper2d=True
  else:
    no_localizer=True

  if (navigation=='gradient_based_nav'):
    run_gradient_based_navigation=True
  elif (navigation=='move_base'):
    run_move_base=True
  elif (navigation=='move_base+grad_nav'):
    run_move_base_grad_nav=True
  elif (navigation=='spqrel_planner'):
    run_spqrel_planner=True
  elif (navigation=='spqrel_planner+grad_nav'):
    run_spqrel_planner_grad_nav=True
  elif (navigation=='explorer'):
    run_explorer=True
    run_move_base=True


  amcl_str = ''
  if (run_amcl):
    amcl_str = "use_amcl:=true"

  thin_localizer_str = ''
  if (run_thin_localizer):
    thin_localizer_str = "use_thin_localizer:=true"

  srrg_localizer_str = ''
  if (run_srrg_localizer):
    srrg_localizer_str = "use_srrg_localizer:=true"

  no_localizer_str = ''
  if (no_localizer):
    no_localizer_str = "use_map_server:=false"

  gmapping_str = ''
  if (run_gmapping):
    gmapping_str = "use_gmapping:=true"
    no_localizer_str = "use_map_server:=false"

  cg_mrslam_str = ''
  if (run_cg_mrslam):
    cg_mrslam_str = "use_cg_mrslam:=true"
    no_localizer_str = "use_map_server:=false"

  srrg_mapper2d_str = ''
  if (run_srrg_mapper2d):
    srrg_mapper2d_str = "use_srrg_mapper2d:=true"
    no_localizer_str = "use_map_server:=false"

  # Choose one navigation method
  nav_str = ''
  use_grad_nav = False
  if (run_gradient_based_navigation):
    nav_str = "use_gradient_based_navigation:=true"
    use_grad_nav = True
  elif (run_move_base):
    nav_str = "use_move_base:=true"
  elif (run_move_base_grad_nav):
    nav_str = "use_move_base_grad_nav:=true"
    use_grad_nav = True
  elif (run_spqrel_planner):
    nav_str = "use_spqrel_planner:=true"
  elif (run_spqrel_planner_grad_nav):
    nav_str = "use_spqrel_planner_grad_nav:=true"
    use_grad_nav = True

  explorer_str = ''
  if (run_explorer):
    explorer_str = "use_explorer:=true"

  if (localization!='none' or navigation!='none'):
    cmd = 'roslaunch stage_environments stage_robot.launch robotname:=%s robotid:=%s nrobots:=%s mapdir:=%s mapname:=%s initial_pose_x:=%f initial_pose_y:=%f initial_pose_a:=%f laser_topic:=%s laser_frame:=%s %s %s %s %s %s %s %s %s %s' % (robot_name, robot_id, nrobots, mapdir, map_name, INITPOSE_X, INITPOSE_Y, INITPOSE_TH_RAD, LASER_TOPIC, LASER_FRAME, amcl_str, thin_localizer_str, srrg_localizer_str, gmapping_str, cg_mrslam_str, srrg_mapper2d_str, nav_str, explorer_str, no_localizer_str )
    send_command(cmd, 'robot')
    os.system('sleep 10')

  # Start the demo
  if (demo=='hri_pnp'):
    cmd = 'roslaunch --wait hri_pnp hri_pnpas.launch robot_name:=%s' % (robot_name)
    send_command(cmd, demo)
    os.system('sleep 1')
  elif (demo=='rcathome'):
    cmd = 'roslaunch --wait robocupathome_pnp rcathome_pnp.launch robot_name:=%s' % (robot_name)
    send_command(cmd, demo)
    os.system('sleep 3')
    cmd = 'roslaunch --wait rosplan_demos pnp_conditional.launch'
    send_command(cmd, 'rosplan')
    os.system('sleep 3')
  elif (demo=='saman2'):
    cmd = 'roslaunch --wait saman2_pnpas saman2_pnp.launch robot_name:=%s' % (robot_name)
    send_command(cmd, demo)
    os.system('sleep 3')



def start_tools_robot(robot_name, rviz, demo, joystick, keyboard):

  # Start rviz
  if (rviz==1):
    cmd = 'rosrun rviz rviz -d `rospack find stage_environments`/config/%s/rviz/%s.rviz' % (robot_name, robot_name)
    send_command(cmd, 'rviz')
    os.system('sleep 3')

  # Drive the robot with joystick or keyboard
  if (joystick==1) or (keyboard==1):
    cmd = 'roslaunch --wait stage_environments joystick.launch robot_name:=%s ' % (robot_name)
    if (keyboard==1):
      cmd = cmd + ' use_keyboard:=true '
    if (not use_grad_nav): # send cmd_vel directly
      cmd = cmd + ' cmd_vel_topic:=cmd_vel '
    send_command(cmd, 'joystick')
    os.system('sleep 3')


def start_tools(robot, nrobots, rviz, demo, joystick, keyboard):

  for i in range(0,int(nrobots)):
    robot_name = robot+'_'+str(i)
    robot_id = i

    start_tools_robot(robot_name, rviz, demo, joystick, keyboard)


def start_stage(mapdir, map_name, robot, nrobots, init, stageros_args):

  print('Stage: map:%s robot:%s nrobots:%d' %(map_name,robot,nrobots))
  print('stage_ros args: %s' %stageros_args)

  if (robot=='sapienzbot'):
    robottype='erratic'
  elif (robot=='diago' ):
    robottype='segway'
  else:
    robottype=robot

  output_worldfile="%s/AUTOGEN_%s_%s_%d.world" %(mapdir,map_name,robot,nrobots)

  # Create the world
  cmd = 'rosrun stage_environments create_world.py %s %s %s %s %.2f %.2f %.2f %s' % (map_name, robottype, robot, nrobots, init[0], init[1], init[2], output_worldfile)
  print(cmd)
  os.system(cmd)

  # Start the simulated environment

  cmd = 'roslaunch --wait stage_environments stage_map.launch world_file:=%s robot_type:=%s laser_topic:=%s laser_frame:=%s mapdir:=%s mapname:=%s stageros_args:=%s' % (output_worldfile, robottype, LASER_TOPIC_STAGE, LASER_FRAME, mapdir, map_name, stageros_args)
  send_command(cmd, 'stage')
  os.system('sleep 5')

  print("\nMap information\n")
  print("mapsdir:=%s map_name:=%s" %(mapdir,map_name))

  print("\nLocalization information\n")
  print("initial_pose_x:=%.2f initial_pose_y:=%.2f initial_pose_a:=%.2f\n" %( 
       init[0],init[1],init[2]*math.pi/180.0))



def start_robots(mapdir, map_name, robot, nrobots, init, localization, navigation, demo):

  # Start the robot(s)

  for i in range(0,int(nrobots)):
    robot_name = robot+'_'+str(i)
    robot_id = i
    
    ix = init[0] + i%2;
    iy = init[1] + i/2;
    ith_rad = init[2]*math.pi/180.0 # rad

    start_robot(robot_name, robot_id, nrobots, mapdir, map_name, localization, navigation, ix, iy, ith_rad, demo)


def checkDisplay():
  # TODO Check DISPLAY environment if GUI enabled
  
  dispenv = os.getenv("DISPLAY")

  if dispenv is not None and len(dispenv)>=2:
    print("DISPLAY environment %s" %(dispenv))
  else:
    print("No DISPLAY environment !!!")
    #sys.exit(1) 

  # ps aux | grep X
  # .... /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
  ftmp = '/tmp/Xout.txt'
  os.system('ps aux | grep X > %s' %ftmp)
  f = open(ftmp,'r')
  l = f.readline()
  dispstr = None
  while l is not None and l!='' and dispstr is None:
    try:
      i = l.index("Xorg -core")
    except:
      i = -1
    if i>0:
      j = l[i+11:].index(" ")
      dispstr = l[i+11:i+11+j]      
    l = f.readline()
  f.close()
  #os.system('rm %s' %ftmp)

  print("DISPLAY from X process: %s" %dispstr)

  if dispenv != dispstr and dispstr is not None:
    print("Setting DISPLAY to %s" %dispstr)
    os.putenv("DISPLAY", dispstr)


def run_simulation(stageros_args,map_name,robot,nrobots,initpose,localization,navigation,demo,joystick,keyboard,rviz):

  Maps = readmaps.listmaps()

  if not map_name in Maps.keys():
    print("ERROR. Map %s not found." %map_name)
    return

  print('Run simulation: map:%s robot:%s nrobots:%s init:%s loc:%s nav:%s demo:%s joy:%s keyb:%s rviz:%s' % (map_name,robot,nrobots,initpose,localization,navigation,demo,joystick,keyboard,rviz))

  send_command('roscore','roscore')

  mapdir = Maps[map_name]

  if initpose!=None:
      pp = initpose.split()
      init = [0,0,0]
      init[0]=float(pp[0])
      init[1]=float(pp[1])
      init[2]=float(pp[2]) # deg
  else:
      mapfile = "%s/%s.yaml" %(mapdir,map_name)
      mapinfo = readmaps.read_mapinfo(mapfile)
      init = readmaps.get_initpose(mapinfo) # default [0,0,0]

  start_stage(mapdir, map_name, robot, nrobots, init, stageros_args)
  start_robots(mapdir, map_name, robot, nrobots, init, localization, navigation, demo)
  start_tools(robot, nrobots, rviz, demo, joystick, keyboard)




##############
#    GUI
##############

class DIP(tk.Frame):

    def __init__(self, parent):
        tk.Frame.__init__(self, parent) 
        self.parent = parent        
        self.first_map_selected = True
        self.initUI()
        self.Maps = readmaps.listmaps()

	
    def initUI(self):

        appbuttons = False # buttons for other app (loc, nav, joystick, rviz,...)

        self.loadOldConfig()

        self.parent.title("Simulation Launcher")
        self.style = Style()
        self.style.theme_use("alt")
        self.parent.resizable(width=FALSE, height=FALSE)
        self.pack(fill=BOTH, expand=1)
        
        #self.columnconfigure(1, weight=1)
        #self.columnconfigure(3, pad=7)
        #self.rowconfigure(3, weight=1)
        #self.rowconfigure(7, pad=7)
        
        self.map_name_ddm = StringVar(self)
        self.robot_ddm = StringVar(self)
        self.nrobots_ddm = StringVar(self)
        self.initpose_ddm = StringVar(self)

        _row = 0
        # Map
        lbl = Label(self, text="Map")
        lbl.grid(sticky=W, row=_row, column=0, pady=4, padx=5)                
        self.map_name_list = sorted(self.Maps.keys())
        self.map_name_ddm.trace("w", self.map_selected)
        try:
            lastmap_name=self.oldConfigs["map"]
        except:
            lastmap_name=self.map_name_list[0]
        self.map_name_ddm.set(lastmap_name)
        tk.OptionMenu(self, self.map_name_ddm, *self.map_name_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)


        _row = _row + 1
        # Robot type
        lbl = Label(self, text="Robot & Pose")
        lbl.grid(sticky=W, row = _row, column= 0, pady=4, padx=5)
        self.robot_list = Robots
        try:
            lastrobot=self.oldConfigs["robot"]
        except:
            lastrobot=self.robot_list[0]
        self.robot_ddm.set(lastrobot)
        tk.OptionMenu(self, self.robot_ddm, *self.robot_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)

        _row = _row + 1
        # N. Robots
        lbl = Label(self, text="Number of Robots")
        lbl.grid(sticky=W, row = _row, column= 0, pady=4, padx=5)
        self.nrobots_list = Nrobots
        try:
            lastnrobots=self.oldConfigs["nrobots"]
        except:
            lastnrobots=self.nrobots_list[0]
        self.nrobots_ddm.set(lastnrobots)
        tk.OptionMenu(self, self.nrobots_ddm, *self.nrobots_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)

        _row = _row + 1
        # Localization
        lbl = Label(self, text="Localization")  
        if appbuttons:
            lbl.grid(sticky=W, row = _row, column= 0, pady=4, padx=5)
        self.loc_list = Localization
        self.loc_ddm = StringVar(self)
        try:
            lastlocmode=self.oldConfigs["localization"]
        except:
            lastlocmode=self.loc_list[0]
        self.loc_ddm.set(lastlocmode)
        if appbuttons:
            tk.OptionMenu(self, self.loc_ddm, *self.loc_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)


        _row = _row + 1
        # Navigation
        lbl = Label(self, text="Navigation")
        if appbuttons:
            lbl.grid(sticky=W, row=_row, column=0, pady=4, padx=5)
        self.nav_list = Navigation
        self.nav_ddm = StringVar(self)
        try:
            lastnav=self.oldConfigs["navigation"]
        except:
            lastnav=self.nav_list[0]
        self.nav_ddm.set(lastnav)
        if appbuttons:
            tk.OptionMenu(self, self.nav_ddm, *self.nav_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)


        _row = _row + 1
        # Demo
        lbl = Label(self, text="Demo")
        if appbuttons:
            lbl.grid(sticky=W, row=_row, column=0, pady=4, padx=5)
        self.demo_list = Demo
        self.demo_ddm = StringVar(self)
        try:
            lastterm=self.oldConfigs["demo"]
        except:
            lastterm=self.demo_list[0]
        self.demo_ddm.set(lastterm)
        if appbuttons:
            tk.OptionMenu(self, self.demo_ddm, *self.demo_list).grid(sticky=W, row=_row, column=1, pady=4, padx=5)


        _row = _row + 1
        # Joystick
        self.joystick_ddm = IntVar(self)
        try:
          last_joystick=self.oldConfigs["joystick"]
        except:
          last_joystick=0
        self.joystick_ddm.set(last_joystick)
        if appbuttons:
            self.Chk_joystick = tk.Checkbutton(self, text='Joystick', variable=self.joystick_ddm)      
            self.Chk_joystick.grid(sticky=W, row=_row, column=0, pady=10, padx=5)

        # Keyboard
        self.keyboard_ddm = IntVar(self)
        try:
          last_keyboard=self.oldConfigs["keyboard"]
        except:
          last_keyboard=0
        self.keyboard_ddm.set(last_keyboard)
        if appbuttons:
            self.Chk_keyboard = tk.Checkbutton(self, text='Keyboard', variable=self.keyboard_ddm)      
            self.Chk_keyboard.grid(sticky=W, row=_row, column=1, pady=10, padx=5)

        # Rviz
        self.rviz_ddm = IntVar(self)
        try:
          last_rviz=self.oldConfigs["rviz"]
        except:
          last_rviz=0
        self.rviz_ddm.set(last_rviz)
        if appbuttons:
            self.Chk_rviz = tk.Checkbutton(self, text='Rviz', variable=self.rviz_ddm)      
            self.Chk_rviz.grid(sticky=W, row=_row, column=2, pady=10, padx=5)


        _row = _row + 1
        # Buttons
        launchButton = Button(self, text="Start",command=self.launch_script)
        launchButton.grid(sticky=W, row=_row, column=0, pady=4, padx=5)
        
        launchButton = Button(self, text="Quit",command=self.kill_demo)
        launchButton.grid(sticky=W, row=_row, column=1, pady=4, padx=5)
    
    # callback when map is selected
    # find initposes
    def map_selected(self, *args):
        current_map = self.map_name_ddm.get()
        #print 'Current map: ',current_map
        mapdir = self.Maps[current_map]
        mapfile = "%s/%s.yaml" %(mapdir,current_map)
        mapinfo = readmaps.read_mapinfo(mapfile)
        self.initpose_list = readmaps.get_initposes_str(mapinfo)
        #print self.initpose_list
        if (self.first_map_selected):
          self.first_map_selected = False
          try:
            last_init=self.oldConfigs["initpose"]
          except:
            last_init=self.initpose_list[0]
        else:
            last_init=self.initpose_list[0]
        self.initpose_ddm.set(last_init)
        _row = 1
        _col = 2
        self.opt_poses = tk.OptionMenu(self, self.initpose_ddm, *self.initpose_list)
        self.opt_poses.grid(sticky=W, row=_row, column=_col, pady=4, padx=5)

    
    
    def launch_script(self):
        self.saveConfigFile();
        stageros_args = ""
        thread.start_new_thread( run_simulation, (stageros_args, self.map_name_ddm.get(), self.robot_ddm.get(), int(self.nrobots_ddm.get()), self.initpose_ddm.get(), self.loc_ddm.get(), self.nav_ddm.get(), self.demo_ddm.get(), self.joystick_ddm.get(), self.keyboard_ddm.get(), self.rviz_ddm.get()) )

    
    def quit(self):
      self.saveConfigFile()
      #self.parent.destroy()
      
    def kill_demo(self):
      os.system("rosrun stage_environments quit.sh &")
      
      
    def saveConfigFile(self):
      f = open('lastConfigUsed', 'w')
      f.write("[Config]\n")
      f.write("map: %s\n"%self.map_name_ddm.get())
      f.write("robot: %s\n"%self.robot_ddm.get())
      f.write("nrobots: %s\n"%self.nrobots_ddm.get())
      f.write("initpose: %s\n"%self.initpose_ddm.get())
      f.write("localization: %s\n"%self.loc_ddm.get())
      f.write("navigation: %s\n"%self.nav_ddm.get())
      f.write("demo: %s\n"%self.demo_ddm.get())
      f.write("joystick: %s\n"%self.joystick_ddm.get())
      f.write("keyboard: %s\n"%self.keyboard_ddm.get())
      f.write("rviz: %s\n"%self.rviz_ddm.get())
      f.close()


    def loadOldConfig(self):
      try:
        self.oldConfigs = {}
        self.Config = ConfigParser.ConfigParser()
        self.Config.read("lastConfigUsed")
        for option in self.Config.options("Config"):
          self.oldConfigs[option] = self.Config.get("Config", option)
      except:
        print("Could not load config file")


def run_server(port):

    # Default values
    stageros_args = ""
    map_name = ""
    robot_type_default = "orazio"
    num_robots_default = 1
    initpose = None
    localization = None
    navigation = None
    demo = None
    joystick = False
    keyboard = False
    rviz = False

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #sock.settimeout(3)
    # Bind the socket to the port
    server_address = ('', port)
    sock.bind(server_address)
    sock.listen(1)
    print("Stage_environments server started on port %d ..." %port)


    connected = False
    dorun = True
    while dorun:

        if not connected:
            print("-- Waiting for connection ...")
        while (dorun and not connected):
            try:
                # Wait for a connection
                connection, client_address = sock.accept()
                connected = True
                print ('-- Connection from %s'  %client_address[0])
            except KeyboardInterrupt:
                print("User interrupt (quit)")
                dorun = False
            except Exception as e:
                print("Connection error")
                print(e)
                pass # keep listening
    
        if not dorun:
            return

        # print("-- Waiting for data...")
        data = None
        while dorun and connected and data is None:
            # receive data
            try:
                #connection.settimeout(3) # timeout when listening (exit with CTRL+C)
                data = connection.recv(320)  # blocking
                data = data.strip()
            except KeyboardInterrupt:
                print("User interrupt (quit)")
                dorun = False
            except socket.timeout:
                data = None
                print("socket timeout")

        if data is not None:
            if len(data)==0:
                connected = False
            else:
                print(data)
                if data=='@quitserver':
                    dorun = False
                elif data=='@stagekill':
                    os.system("rosnode kill map_server stageros")
                    time.sleep(5)
                    cmd = "kill $(ps aux | grep xterm | grep ros | grep -v grep | awk '{print $2}')"
                    os.system(cmd)
                    os.system('rosparam set /use_sim_time false')
                elif data=='@quitall':
                    os.system("rosrun stage_environments quit.sh")
                else: # <map_name>;<robot_type>;<num_robots>
                    v = data.split(';')
                    print(v)
                    print(len(v))
                    map_name=v[0]
                    robot_type=robot_type_default
                    num_robots=num_robots_default
                    if len(v)>1:
                        robot_type=v[1]
                    if len(v)>2:
                        num_robots=int(v[2])
                    run_simulation(stageros_args,map_name,robot_type,num_robots,
                       initpose,localization,navigation,demo,joystick,keyboard,rviz)


def main():
  global use_tmux

  parser = argparse.ArgumentParser()

  parser.add_argument('--list', default = False, action ='store_true', 
        help='List of available maps')
  parser.add_argument('--server', default = False, action ='store_true', 
        help='Start in server mode')

  parser.add_argument('-server_port', type=int, default=default_port, help='server port')

  parser.add_argument("map_name", type=str, default=None,  nargs='?',                  
        help="Name of map")
  parser.add_argument("robot_type", type=str, default='orazio',  nargs='?',                  
        help="Type of robot (default: orazio)")
  parser.add_argument("num_robots", type=int, default=1,  nargs='?',                  
        help="Number of robots (default: 1)")
  parser.add_argument("initpose", type=str, default=None,  nargs='?',                  
        help="Init pose in the form 'X Y Th' (default: None)")
  parser.add_argument("localization", type=str, default=None,  nargs='?',                  
        help="Localization method (e.g., 'amcl') (default: None)")
  parser.add_argument("navigation", type=str, default=None,  nargs='?',                  
        help="Navigation method (e.g., 'move_base') (default: None)")
  parser.add_argument("demo", type=str, default=None,  nargs='?',                  
        help="Demo script (default: None)")
  parser.add_argument('--joystick', default = False, action ='store_true', 
        help='Use joystick for teleop')
  parser.add_argument('--keyboard', default = False, action ='store_true', 
        help='Use keyboard for teleop')
  parser.add_argument('--rviz', default = False, action ='store_true', 
        help='Launch rviz')
  parser.add_argument('--tmux', default = False, action ='store_true', 
        help='Use tmux instead of xterm to launch commands')
  parser.add_argument('--no_gui', default = False, action ='store_true', 
        help='Run stage without GUI')

  args = parser.parse_args()

  if args.tmux:
    use_tmux = os.system('tmux -V')==0  # check if it exists

  if (args.server):
    print("Starting in server mode ...")
    run_server(args.server_port)
    sys.exit(0)

  elif (len(sys.argv)==1):
    root = tk.Tk()
    f = DIP(root)
    root.geometry("420x340+0+0")    
    root.mainloop()
    f.quit()
    sys.exit(0)

  elif (args.list):
    Maps = readmaps.listmaps()
    cnt = 0
    for m in sorted(Maps.keys()):
        print(m, end='')
        k = len(m)
        if (k<8): 
            print('', end='\t\t\t')
        elif (k<16): 
            print('', end='\t\t')
        else: 
            print('', end='\t')
        cnt += 1
        if cnt==4:
            print('')
            cnt = 0
    print('\n')
    sys.exit(0)


  if args.map_name != None:

    stageros_args = ""
    if args.no_gui:
        stageros_args = "-g"

    run_simulation(stageros_args,args.map_name,args.robot_type,args.num_robots,
        args.initpose,args.localization,args.navigation,
        args.demo,args.joystick,args.keyboard,args.rviz)

    print("Use the following command to quit the simulation")
    print("  rosrun stage_environments quit.sh")

  else:

    print("ERROR. Cannot start simulation because map_name is missing!!!")



if __name__ == '__main__':
    main()

