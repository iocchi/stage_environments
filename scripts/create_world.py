#!/usr/bin/python

import sys
import readmaps

vcolors = ['DarkRed', 'blue', 'green', 'magenta', 'cyan', 'yellow']

def usage():
    print "  Use: ",sys.argv[0],' <map_name> <robot_type> <robot_basename> <num_robots> [init_x init_y init_th] <output_world_file>'
    print "  e.g. ",sys.argv[0],' disB1 diago diago 1 dis_B1_diago.world'

if __name__ == '__main__':
  if (len(sys.argv)!=6 and len(sys.argv)!=9):
    usage()
    sys.exit(1)

  map_name = sys.argv[1]
  robot_type = sys.argv[2]
  robot_name = sys.argv[3]  # basename of the robot
  nrobots = int(sys.argv[4])
  init = None
  if (len(sys.argv)==9):
    init = [0,0,0]
    init[0] = float(sys.argv[5])
    init[1] = float(sys.argv[6])
    init[2] = float(sys.argv[7])

  outworld = sys.argv[-1]

  maps = readmaps.listmaps()
  if map_name not in maps.keys():
    print("Map %s not found.")
    sys.exit(-1)

  mapdir = maps[map_name]
  mapfile = "%s/%s.yaml" %(mapdir,map_name)
  mapinfo = readmaps.read_mapinfo(mapfile)
  floorplan = readmaps.get_floorplan_str(mapinfo, mapdir)

  if init is None:
    init = readmaps.get_initpose(mapinfo) # default [0,0,0]

  f = open(outworld,'w')

  f.write('include "include/floorplan.inc"\n')
  f.write('include "include/box.inc"\n')
  f.write('include "include/person.inc"\n')
  f.write('include "include/%s.inc"\n\n' %robot_type )

  f.write('window (\n  size   [ 600 500 1 ]\n  rotate [ 0.000 0.000 ]')
  f.write('  center [ %.1f %.1f ]\n  scale 50\n  show_data 0\n)\n\n' %(init[0],init[1]))

  f.write('%s\n' %floorplan)

  # write robots
  ncolors = len(vcolors)
  for i in range(0,nrobots):
    ix = init[0]+(i%2)
    iy = init[1]-(i/2)
    f.write('%s( pose [ %.2f %.2f 0 %.2f ] name "%s%d" color "%s")\n'% (robot_type, ix, iy, init[2], robot_name+'_', i, vcolors[i%ncolors]))

  f.write('\n')

  # write semantic elements

  for k in mapinfo.keys():
    v = mapinfo[k]
    if type(v)== dict and 'type' in v.keys():
        tp = v['type']
        if tp=='door' or tp=='person' or tp[0:3]=='box' or tp[0:3]=='tag':
            sm = "%s( " %tp
            sm += " name \"%s\" " %(k)
            for kv in v.keys():
                vv = v[kv]
                if kv=='str':
                    sm += " %s " %(vv)
                elif kv=='light' or kv=='body':
                    sm += " %s ( " %(kv)
                    for kkv in vv:
                        vvv = vv[kkv]
                        if type(vvv)==list:  # for lists remove ','
                            sv = "%r" %vvv
                            vvv = sv.replace(',',' ')
                            sm += " %s %s " %(kkv, vvv)
                        elif type(vvv)==str:
                            sm += " %s \"%s\" " %(kkv, vvv)
                    sm += " ) "
                elif kv=='pose' or kv=='size' or kv=='color' or kv=='name':
                    if type(vv)==list:  # for lists remove ','
                        sv = "%r" %vv
                        vv = sv.replace(',',' ')
                        sm += " %s %s " %(kv, vv)
                    elif type(vv)==str:
                        sm += " %s \"%s\" " %(kv, vv)
                    else:
                        sm += " %s %r " %(kv, vv)
                else:
                    pass
            sm += ")"
            #print(sm)

            f.write(sm)

            f.write('\n')

  f.write('\n')

  f.close()

  print("File %s written.\n" % outworld)


