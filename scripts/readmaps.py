# -*- coding: utf-8 -*-

import os, sys
import yaml

def getmapsdirs():
    v = os.getenv('MAPSDIR')
    if v==None:
        print("No maps dir. Plaese set MAPSDIR environment variable.")
        default_mapsdir = '../maps'
        print("Using default value: %s" %default_mapsdir)
        return [default_mapsdir]
    else:
        return v.split(':')

def getmaps(dd, maps):
    # r=root, d=directories, f = files
    for r, d, f in os.walk(dd):
        for fi in f:
            if '.yaml' in fi:
                #print('  '+fi)
                k = fi.find('.yaml')
                m = fi[0:k]
                maps[m] = r



def listmaps():
    maps = {}
    dd = getmapsdirs()
    for d in dd:
        mm = getmaps(d, maps)
    return maps


def read_mapinfo(mapyaml):
    mapinfo = {}
    try:
        with open(mapyaml, 'r') as f:
            mapinfo = yaml.safe_load(f)
    except Exception as e:
        print(e)
    return mapinfo


def get_floorplan_str(mapinfo, mdir):

    image = mapinfo['image']
    r = mapinfo['resolution']
    occ_thr = mapinfo['occupied_thresh']
    free_thr = mapinfo['free_thresh']
    orig = mapinfo['origin']
    blx = orig[0]
    bly = orig[1]

    os.system('file %s/%s > /tmp/fileimage.out' %(mdir,image))
    with open('/tmp/fileimage.out') as f:
        l = f.read()
        k1 = l.find(' x ')
        k2 = l[k1:].find(',')
        #print(l[k1-4:k1+k2])
        v = l[k1-4:k1+k2].split('x')
        W = int(v[0])
        H = int(v[1])
        #print("Image size: %d x %d" %(W,H))

    Ou = -blx/r
    Ov = H + bly/r


    #print('\nMap file name: %s' %image)
    #print('Width [pixel]: %d' %W)
    #print('Height [pixel]: %d' %H)
    #print('Resolution [m/pixel]: %f' %r)
    #print('Origin X [pixel]: %d' %Ou)
    #print('Origin Y [pixel]: %d\n' %Ov)



    # world size
    wr = W * r
    hr = H * r
    # bottom-left coords
    blx = -Ou * r
    bly = -(H-Ov) * r
    # center coords
    cx = (wr/2 + blx)
    cy = (hr/2 + bly)

    return "floorplan (\n  size [%f %f 2]\n  pose [%f %f 0 0]\n  bitmap \"%s/%s\"\n)\n" %(wr,hr,cx,cy,mdir,image)

    #print("image: %s\n"
    #      "resolution: %f\n"
    #      "origin: [%f, %f, 0]\n"
    #      "negate: 0\n"
    #      "occupied_thresh: %.2f\n"
    #      "free_thresh: %.3f\n" %(image,r,blx,bly,occ_thr,free_thr))


def get_initposes_str(mapinfo):
  r = ['0 0 0']
  if 'initposes' in mapinfo.keys():
    r = []
    pp = mapinfo['initposes']
    for p in pp:
        r.append("%g %g %g" %(p[0],p[1],p[2]))
  return r


def get_initpose(mapinfo):
  r = [0, 0, 0]
  if 'initposes' in mapinfo.keys():
      initposes = mapinfo['initposes']
      if (len(initposes)>0):
        r = initposes[0]
  return r


if __name__=='__main__':

    maps = listmaps()
    print maps
    print (len(maps))

    m = maps.keys()[0]
    d = maps[m]

    mapfile = "%s/%s.yaml" %(d,m)

    mi = read_mapinfo(mapfile)
    print(get_floorplan_str(mi,d))

    print(get_initpose(mi))

