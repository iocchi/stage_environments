#!/bin/bash

if [ "$1" == "nvidia" ]; then
    awk '{gsub(/runc/, "nvidia")} 1' docker-compose.yml > _dc.yml
else
    cp docker-compose.yml _dc.yml
fi

docker-compose -f _dc.yml up

