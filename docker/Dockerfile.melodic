# Docker file for stage_environments
# Luca Iocchi, DIAG, Sapienza University of Rome, Italy
# 2020-2021

FROM ros:melodic-ros-base-bionic

ARG DEBIAN_FRONTEND=noninteractive
ARG NVIDIADIR=none
ARG UID=1000
ARG GID=1000

###### USER root ######

# install libraries and ros packages 

# RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

RUN apt-get update && \
    apt-get install -y -qq --no-install-recommends \
        file less sudo eom wget nano unzip tmux xterm \
        iputils-ping net-tools openssh-client \
        fluid libfltk1.3-dev libjpeg-dev libpng-dev libglu1-mesa-dev libltdl-dev \
        python-tk

RUN apt-get install -y -qq --no-install-recommends \
        ros-melodic-desktop ros-melodic-stage-ros ros-melodic-map-server && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# User: robot (password: robot) with sudo power

RUN useradd -ms /bin/bash robot && echo "robot:robot" | chpasswd && adduser robot sudo
RUN usermod -u $UID robot && groupmod -g $GID robot

###### USER robot ######

USER robot

RUN echo "set -g mouse on" > $HOME/.tmux.conf 
RUN touch ~/.sudo_as_admin_successful

# ROS workspace

RUN mkdir -p $HOME/ros/catkin_ws/src

RUN /bin/bash -c "source /opt/ros/melodic/setup.bash; cd $HOME/ros/catkin_ws/src; catkin_init_workspace; cd ..; catkin_make"

RUN echo "source $HOME/ros/catkin_ws/devel/setup.bash\n" >> $HOME/.bashrc

RUN rosdep update


# Nvidia drivers

RUN if [ "$NVIDIADIR" != "none" ]; then \
      echo "if [ -d $NVIDIADIR ]; then" >> $HOME/.bashrc && \
      echo "  export PATH=\"$NVIDIADIR/bin:\${PATH}\"" >> $HOME/.bashrc && \
      echo "  export LD_LIBRARY_PATH=\"$NVIDIADIR:\${LD_LIBRARY_PATH}\" " >> $HOME/.bashrc && \
      echo "fi" >> $HOME/.bashrc ; \
    fi


# Trick to disable cache from here
#ADD http://worldclockapi.com/api/json/utc/now /tmp/time.tmp 

ARG FORCEBUILD=none

RUN echo "$FORCEBUILD"

### New Stage ###

RUN mkdir -p $HOME/src/

RUN cd $HOME/src && \
    git clone https://github.com/iocchi/Stage.git && \
    mkdir -p Stage/build && cd Stage/build && \
    cmake .. && make

USER root

RUN cd /home/robot/src/Stage/build && make install

USER robot

RUN echo "export CMAKE_PREFIX_PATH=/usr/local/lib64/cmake:\$CMAKE_PREFIX_PATH\n" >> $HOME/.bashrc


### Additional packages ###

# stage_environments

# Trick to disable cache from here
#ADD http://worldclockapi.com/api/json/utc/now /tmp/time.tmp 

RUN mkdir -p $HOME/src && \
    cd $HOME/src && \
    git clone https://bitbucket.org/iocchi/stage_environments.git

RUN cd $HOME/ros/catkin_ws/src && \
    ln -s $HOME/src/stage_environments . 

RUN echo "if [ \"\$MAPSDIR\" == \"\" ]; then"  >> $HOME/.bashrc
RUN echo "  export MAPSDIR=\$HOME/src/stage_environments/maps" >> $HOME/.bashrc
RUN echo "fi\n"  >> $HOME/.bashrc

# Compile ROS packages

RUN /bin/bash -ci "source $HOME/.bashrc; cd $HOME/ros/catkin_ws; catkin_make"


# Set working dir and container command

WORKDIR /home/robot

CMD bash -ci 'rosrun stage_environments start_simulation.py'

