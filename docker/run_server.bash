#!/bin/bash

# Use: ./run_server.bash [melodic|kinetic]

NVIDIA_STR=""

NVIDIADIR=`ls -d1 /usr/lib/nvidia-* 2> /dev/null | head -n 1`

if [ $NVIDIADIR ]; then
  echo "Found nvidia libraries: $NVIDIADIR"
  NVIDIA_STR="-v $NVIDIADIR:$NVIDIADIR \
              --device /dev/dri"
fi

STAGEENV_DIR=`pwd | gawk '{ print gensub(/\/docker/, "", 1) }'`
STAGE_DIR=`echo $STAGEENV_DIR | gawk '{ print gensub(/stage_environments/, "Stage", 1) }'`

STAGEENV_MOUNT=""
if [[ $STAGEENV_DIR == *"stage_environments" ]]; then
  echo "Mounting $STAGEENV_DIR"
  STAGEENV_MOUNT="-v $STAGEENV_DIR:/home/robot/src/stage_environments"
fi

STAGE_MOUNT=""
if [[ $STAGE_DIR == *"Stage" ]]; then
  echo "Mounting $STAGE_DIR"
  STAGE_MOUNT="-v $STAGE_DIR:/home/robot/src/Stage"
fi


VERSION=latest
if [ ! "$1" == "" ]; then
  VERSION=$1
fi

docker run -it -d \
    --name stage --rm \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
    -v $HOME/.Xauthority:/home/robot/.Xauthority:rw \
    $NVIDIA_STR \
    -e DISPLAY=$DISPLAY \
    -e QT_X11_NO_MITSHM=1 \
    --privileged \
    --net=host \
    $STAGEENV_MOUNT \
    $STAGE_MOUNT \
    iocchi/stage_environments \
    bash -ci "rosrun stage_environments start_simulation.py --server -server_port 9235"



