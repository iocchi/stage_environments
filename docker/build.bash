#!/bin/bash

# Use: ./build.bash [melodic|kinetic] [forcebuildtag]

UPAR="--build-arg UID=`id -u` --build-arg GID=`id -g`"

VERSION=melodic
if [ ! "$1" == "" ]; then
  VERSION=$1
fi

FORCEBUILDTAG=""
if [ ! "$2" == "" ]; then
  FORCEBUILDTAG="--build-arg FORCEBUILD=$2"
fi

docker build $UPAR $FORCEBUILDTAG -t iocchi/stage_environments:$VERSION -f Dockerfile.$VERSION .

if [ "$VERSION" == "melodic" ]; then
  docker tag iocchi/stage_environments:melodic iocchi/stage_environments:latest
fi

# https://hub.docker.com/repository/docker/iocchi/stage_environments
# docker login
# docker push iocchi/stage_environments:melodic
# docker push iocchi/stage_environments:latest

