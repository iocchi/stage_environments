# Update script for docker hub
# https://hub.docker.com/repository/docker/iocchi/stage_environments

# Versions
# 1.0    23/10/2020   first commit
# 1.1    24/10/2020   --no_gui option in start_simulation.py
# 1.1.1  29/10/2020   no-cache trick in Dockerfile
# 1.2    29/11/2020   custom Stage, stageGUIRequest topic, kinetic and melodic versions
# 1.2.1  28/4/2022    updated MAPSDIR in .bashrc
# 1.2.2  30/4/2022    added world tags

VERSION=`cat version.txt`

# Build the last versions of the image
./build.bash melodic
./build.bash kinetic

docker tag iocchi/stage_environments:melodic iocchi/stage_environments:melodic.$VERSION
docker tag iocchi/stage_environments:kinetic iocchi/stage_environments:kinetic.$VERSION
docker tag iocchi/stage_environments:melodic iocchi/stage_environments:latest

# login

docker login

# push

docker push iocchi/stage_environments:melodic.$VERSION
docker push iocchi/stage_environments:kinetic.$VERSION
docker push iocchi/stage_environments:melodic
docker push iocchi/stage_environments:kinetic
docker push iocchi/stage_environments:latest


