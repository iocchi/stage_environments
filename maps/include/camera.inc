define cam160 camera
(
  # camera properties
  resolution [ 160 120 ]
  range [ 0.2 6.0 ]
  fov [ 60.0 40.0 ]
  pantilt [ 0.0 0.0 ]

  # model properties
  size [ 0.03 0.1 0.06 ]
  color "DarkBlue"
)

