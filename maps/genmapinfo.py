

# input
print('Image name, size, resolution and origin')

mapfn = raw_input('Map file name: ')
W = input('Width [pixel]: ')
H = input('Height [pixel]: ')
r = input('Resolution [m/pixel]: ')
Ou = input('Origin X [pixel]: ')
Ov = input('Origin Y [pixel]: ')

# world size
wr = W * r 
hr = H * r
# bottom-left coords
blx = -Ou * r
bly = -(H-Ov) * r
# center coords
cx = (wr/2 + blx) 
cy = (hr/2 + bly)

print("floorplan (\n"
      "  size [%f %f 1]\n"
      "  pose [%f %f 0 0]\n"
      "  bitmap \"%s\"\n"
      ")\n" %(wr,hr,cx,cy,mapfn))

print("image: %s\n"
      "resolution: %f\n"
      "origin: [%f, %f, 0]\n"
      "negate: 0\n"
      "occupied_thresh: 0.65\n"
      "free_thresh: 0.196\n" %(mapfn,r,blx,bly))

